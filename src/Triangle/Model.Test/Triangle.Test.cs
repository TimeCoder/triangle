﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Model.Test
{
	[TestClass]
	public class TriangleTest
	{
		// 1. Создать валидный треугольник 
		// 2. Вычислить площадь
		//
		// => площадь вычислена правильно, исключений нет
		[TestMethod]
		public void CalcSquareTestPositive()
		{
			var triangleMath = new TriangleMath();

			try
			{
				var square = triangleMath.CalcSquare(200, 150, 250);
				Assert.AreEqual(15000, square, double.Epsilon, "Площадь вычислена неправильно.");
			}
			catch (ShapeNotExistException)
			{
				Assert.Fail("Треугольник ошибочно определен как несуществующий.");
			}
		}

		// 1. Создать невозможный треугольник 
		// 2. Вычислить площадь
		//
		// => выброшено исключение TriangleNotExistException
		[TestMethod]
		public void CalcSquareTestExist()
		{
			var triangleMath = new TriangleMath();

			try
			{
				var square = triangleMath.CalcSquare(200, 150, 1000);
				Assert.Fail("Треугольник ошибочно определен как существующий.");
			}
			catch (ShapeNotExistException)
			{
			}
		}
	}
}
