﻿using System;

namespace Model
{
	/// <summary>
	/// Класс математики треугольника
	/// </summary>
	public class TriangleMath
	{
		private readonly double _relTolerance;

		/// <summary>
		/// Через констуктор можно задать относительную точность вычислений
		/// </summary>
		/// <param name="relTolerance"></param>
		public TriangleMath(double relTolerance = 0.00001)
		{
			_relTolerance = relTolerance;
		}

		/// <summary>
		/// Вычислить площадь треугольника
		/// </summary>
		/// <param name="a">Первая сторона</param>
		/// <param name="b">Вторая сторона</param>
		/// <param name="c">Третья сторона</param>
		/// <returns>Площадь</returns>
		public double CalcSquare(double a, double b, double c)
		{
			// Проверка существования треугольника
			var isExist = (a + b > c && a + c > b && b + c > a);
			if (!isExist)
				throw new ShapeNotExistException();

			// Выделяем предположительные катеты (две стороны, которые меньше третьей)
			double cath1 = 0, cath2 = 0, hypo = 0;

			if (a < b)
			{
				cath1 = a;
				cath2 = b < c ? b : c;
				hypo = b > c ? b : c;
			}
			else
			{
				cath1 = b;
				cath2 = a < c ? a : c;
				hypo = a > c ? a : c;
			}

			// Это действительно прямоугольный треугольник?
			var isRightTri = (Math.Abs(cath1*cath1 + cath2*cath2 - hypo*hypo) < a*_relTolerance);
			if (isRightTri)
			{
				// тогда можно считать по точной формуле
				return cath1*cath2*0.5;
			}

			// В общем случае считаем по форуле Герона
			var p = (a + b + c)*0.5;
			return Math.Sqrt(p*(p - a)*(p - b)*(p - c));
		}
	}
}